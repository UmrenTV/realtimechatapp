import firebase from "firebase/app";
import "firebase/database";
import { apikeys } from "./apikeys.js";

const config = apikeys;
// the api keys are coming from firebase app that you should create
// and add them to the /src folder as apikeys.js with contents:
//  export const apikeys = {
//  THE CONFIG FROM FIREBASE goes here, like
//  apiKey: "",
//  authDomain: "",
//  projectId: "",
//  etc...
// }

const db = firebase.initializeApp(config);
export default db;
